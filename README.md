# star-force

Annotated disassembly for the arcade game Star Force, by Tehkan, released in 1984.

Started 2023-11-07.

Progress 2024-02-07:
  26 TODO,   99 Xlabels (90% done), 1062 lines untouched (86% done), 125% commented

Progress 2024-02-11:
  25 TODO,   60 Xlabels (94% done),  625 lines untouched (92% done), 126% commented

Progress 2024-02-16:
  17 TODO,    0 Xlabels (100% done),    0 lines untouched (100% done), 129% commented
